<?php
include('config.php');

$sql = "SELECT spocname, Count(dealtitle) as dealtitle from finallead WHERE date(leaddate) BETWEEN '2018/01/01' AND '2018/01/31' AND `leadstatus`=1 GROUP BY spocname ;";

$result = mysqli_query($conn,$sql);
$label= array();
$color = array("#FF0033","#FF6600");
$tooltip = array('trigger'=>'axis');
$legend = array('data'=>array('A','B','C','D','E'));
$feature = array('magicType' => array('show'=>true,'type'=> array('line','bar')), 'restore' => array('show'=>true),'saveAsImage' => array('show'=>true));
$toolbox = array('show'=>true,'feature'=>$feature);

$i =0;

while($row=mysqli_fetch_array($result)) {
	$label[$i] = $row['spocname'];
	$data[$i] = $row['dealtitle'];
	$i++;
}

$xaxis = array(array('type'=>'category','data'=>$label));

$series = array(array('name'=>'One Month','type'=>'bar','data'=>$data,'markPoint'=>array('data'=>array(array('type'=>'max','name'=>'Max'),array('type'=>'min','name'=>'Min'))),'markLine'=>array('data'=>array(array('type'=>'average','name'=>'Average')))));

$arrDataset = array('tooltip' => $tooltip,'legend' => $legend,'toolbox' => $toolbox ,'color' => $color,'calculable'=>'true','xAxis'=>$xaxis ,'yAxis'=>array(array('type'=>'value')),'series' => $series);

$arrReturn = ($arrDataset);
mysql_close($conn);
echo json_encode($arrReturn);
?>
