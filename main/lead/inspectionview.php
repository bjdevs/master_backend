<?php
ob_start();
session_start();
include_once("../database_config.php");
include_once("../functions.php");
date_default_timezone_set('Asia/Kolkata');
// if(!$_SESSION['id']){
   // header("location:index.php");
   // die;
// }
// $username= $_SESSION['username'];
// $uid = $_SESSION['id'];
// $uemail = $_SESSION['email'];
// $department = $_SESSION['department'];


// $sql_spoc = "SELECT `id`,`username` FROM `tbl_usermaster` WHERE `department` = 3";
// $query_spoc = mysqli_query($conn,$sql_spoc);

// $i = 0;
// while($row1 = mysqli_fetch_array($query_spoc)){
    // $spoc_names[$i] = $row1['username'];
    // $ids[$i] = $row1['id']; 
    // $i++;
// }

if(isset($_POST['btn-fetch']))
{
	
    
}
 else {
    $query = "SELECT a.customerId as customerId,b.customerName as customerName,b.phoneNumber as phoneNumber,a.carMake as carMake,a.carModel as carModel,a.carVariant as carVariant,a.carModel as carModel,a.carId as carId,a.address as address,a.addressType as addressType,a.inspectorId as inspectorId from TBL_SPOC_CAR_DATA a join TBL_LEAD_CAR_DATA b on a.customerId=b.customerId where b.leadStatus=151";
    $search_result = filterTable($query);
}

$sql_spoc = "Select inspectorId as id, inspectorName as name from TBL_INSPECTOR_IDS";
$query_spoc = mysqli_query($conn,$sql_spoc);

$i = 0;
while($row1 = mysqli_fetch_array($query_spoc)){
    $spoc_names[$i] = $row1['name'];
    $ids[$i] = $row1['id']; 
    $i++;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>Bluejack - Lead Dashboard</title>
    <!-- Bootstrap Core CSS -->

    <link rel="preload" href="../modal.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../modal.css"></noscript>

    <link rel="preload" href="../../assets/plugins/bootstrap/css/bootstrap.min.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.min.css"></noscript> 
    <!-- Custom CSS -->
    <link rel="preload" href="../css/style.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../css/style.css"></noscript>

    <link rel="preload" href="../../assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../../assets/plugins/Magnific-Popup-master/dist/magnific-popup.css"></noscript>

    <!-- You can change the theme colors from here -->
    <link rel="preload" href="../css/colors/blue.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../css/colors/blue.css"></noscript>
   
    <link rel="preload" href="../../assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../../assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"></noscript> 

   <script>
        !function(a){"use strict";var b=function(b,c,d){function j(a){if(e.body)return a();setTimeout(function(){j(a)})}function l(){f.addEventListener&&f.removeEventListener("load",l),f.media=d||"all"}var g,e=a.document,f=e.createElement("link");if(c)g=c;else{var h=(e.body||e.getElementsByTagName("head")[0]).childNodes;g=h[h.length-1]}var i=e.styleSheets;f.rel="stylesheet",f.href=b,f.media="only x",j(function(){g.parentNode.insertBefore(f,c?g:g.nextSibling)});var k=function(a){for(var b=f.href,c=i.length;c--;)if(i[c].href===b)return a();setTimeout(function(){k(a)})};return f.addEventListener&&f.addEventListener("load",l),f.onloadcssdefined=k,k(l),f};"undefined"!=typeof exports?exports.loadCSS=b:a.loadCSS=b}("undefined"!=typeof global?global:this);
        !function(a){if(a.loadCSS){var b=loadCSS.relpreload={};if(b.support=function(){try{return a.document.createElement("link").relList.supports("preload")}catch(a){return!1}},b.poly=function(){for(var b=a.document.getElementsByTagName("link"),c=0;c<b.length;c++){var d=b[c];"preload"===d.rel&&"style"===d.getAttribute("as")&&(a.loadCSS(d.href,d,d.getAttribute("media")),d.rel=null)}},!b.support()){b.poly();var c=a.setInterval(b.poly,300);a.addEventListener&&a.addEventListener("load",function(){b.poly(),a.clearInterval(c)}),a.attachEvent&&a.attachEvent("onload",function(){a.clearInterval(c)})}}}(this);
    </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="../../assets/images/Logo_Thumb_White.png" alt="homepage" class="light-logo" width="32px" height="31px" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="../../assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="../../assets/images/textWhite.png" class="light-logo" alt="homepage" width="98px" height="19px" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../../assets/images/users/1.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="../../assets/images/users/1.jpg" alt="user"></div>
                                            <div class="u-text">
                                                <h4><?php echo $username ?></h4>
                                                <p class="text-muted"><?php echo $uemail ?></p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-in"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ================================== 1============================ -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url(../../assets/images/background/user-info.jpg) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="../../assets/images/users/profile.png" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php echo $username ?></a>
                        <div class="dropdown-menu animated flipInY"> <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a> <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a> <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                            <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                            <div class="dropdown-divider"></div> <a href="logout.php" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">BLUEJACK</li>
                        <?php 
							if($department == 3)
							{
								echo '

                                    <li>
                                        <a class="waves-effect waves-dark" aria-expanded="false" href="../lead/leaddashboard.php"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                                    </li>

                                    <li>
										<a class="waves-effect waves-dark" aria-expanded="false" href="../lead/leadassign.php"><i class="mdi mdi-gauge"></i><span class="hide-menu">Lead Assign </span></a>
									</li>
									';
							}
							
						
						?>
						
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">SPOC Task View</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">SPOC Task View</li>
                        </ol>
                    </div>
                    
                </div>


                <div class="row">
                    <div class="col-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Filters</h4>
                            </div>
                            <div class="card-body">
                                    <div class="form-body">
                                        <div class="row ">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label" name="assignedto">Assigned To</label>
                                                    <select class="form-control" style="width: 100%" name="assignto" id="assignto" onchange="filter_data();">
                                                        <option value = "">--SELECT Name--</option>
                                                        <?php 
                                                        for($j=0;$j<sizeof($spoc_names);$j++){
                                                            echo "<option value='" . $ids[$j]."'>".ucwords($spoc_names[$j])."</option>";
                                                        }
                                                        ?>
														
                                                    </select>
                                                </div>
                                            </div>

                                        </div>  
 
                                        <!--</div> -->
                                            <!--/span-->
                                            
                                            <!--/span-->
                                        
                                        
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>


                <div class = "card">
                    <div class = "card-body">
                        <div class="table-responsive m-t-40">
										<form id="frm-example" action="" method="POST">
                                            <table id="myTable" class="table table-bordered table-striped">
                            
                                        <!--<div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">-->
                                                <thead>
                                                    <tr>
                                                        <th>Customer Name</th>
                                                        <th>Phone Number</th>
														<th>Car Make/Model/Variant</th>
                                                        <th>Car Id</th>
                                                        <th>Address</th>
                                                        <th>Inpector Assign</th>
														<th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
													<?php while($row=mysqli_fetch_array($search_result))
											 {
												 echo "
												 <tr>
													<td>".  $row['customerName'] ."</td>
													<td>".  $row['phoneNumber'] ."</td>
													<td><b>Make:</b> ".  $row['carMake'] ." <br/> <b>Model:</b> ". $row['carModel'] ." <br/> <b>Variant:</b> ". $row['carVariant'] ."</td>
													<td>".  $row['carId'] ."</td>
													<td>".  $row['address'] ."</td>
													<td>".  $row['inspectorId'] ."</td>
													<td><select class = 'form-control' id='inspectorName' onchange='assign(this.id);'>
														<option>--Select Name--</option>";
														for($j=0;$j<sizeof($spoc_names);$j++){

                                                            echo "<option value='".$ids[$j]."'>".ucwords($spoc_names[$j])."</option>";
                                                        }
														echo "</select>
													</td>
												</tr>
												 ";
											 }?>
                                                </tbody>
                                            </table>
										
                                    </div>
                                </div>
                            </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                

                <!-- Row -->
                <!-- Row -->
                <!-- Row -->
                <!-- Row -->
                <!-- Row -->
                  
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2017 Bluejack </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/plugins/jquery/jquery.min.js"></script>

    <script type="text/javascript">
        
        function showMessage() {
                
            alert("Clearing set filters.");
            $('#datefrom').val('');
            $('#dateto').val('');
            $("#filterstat").val("");
            var table = $('#myTable').DataTable();
               table.clear().draw();
               //destroy datatable
               //table.destroy();

        }
		
		function countCheck() {
			var assign = document.getElementById('spocname').value
        $("input:checkbox[class=checkid]").each(function () {
			
			if($(this).is(":checked"))
			{
				var id = $(this).val();
				alert(id);
					$.ajax({
					url: 'assignspoc_ajax.php',
					type: 'POST',
					data: {id:id,assign:assign},
					dataType: 'text',
					
					//window.alert(id2);
					success:function(data){
						alert('Successfull');  
					},
					error: function (xhr, ajaxOptions, thrownError) {
						//alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
						//alert("responseText: "+xhr.responseText);
					}
				});
				
			}
            //alert("Id: " + $(this).attr("id") + " Value: " + $(this).val() + " Checked: " + $(this).is(":checked"));
        });
            // var items = document.getElementsByClassName('checkid');
			// for (var i = 0; i < items.length; i++)
				// alert(items[i].name);   
            
        }
		
        $('#datefrom').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
        $('#dateto').bootstrapMaterialDatePicker({ weekStart : 0, time: false });

        function assign(a){
            var name = document.getElementById('inspectorName').value;
			var id = this.id;
            
            $.ajax({
                url: 'assign_lead_to.php',
                type: 'POST',
                data: {carid:carId,name:name},
                dataType: 'text',
                
                //window.alert(id2);
                success:function(data){
                    alert('Successfull');
                    document.getElementById(id).style.display = "none";   
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                    alert("responseText: "+xhr.responseText);
                }
            });

        }
		
		function filter_data()
	   {
	        var id = document.getElementById("assignto").value;
          
		   
		    var table = $('#myTable').DataTable();
		    table.clear().draw();
		    table.destroy();
		   
			$.ajax({
                url: 'filterdata_4.php',
                type: 'POST',
                data: {id:id},
                dataType: 'json',
				
                success:function(result){
					$('#myTable').DataTable( {
						"pageLength": 10,
						"destroy": true,
						"language": {
								"zeroRecords": "No Data Found",
								"infoEmpty": "No Data Found",
							},
					"ajax": "objects3.txt",
						"columns": [
							{ "data": "customerName" },
							{ "data": "phoneNumber" },
							{data: {carMake : "carMake", carModel : "carModel", carVariant : "carVariant"},
                                "width": "50px", "render": function (data, type, row, meta) {
                                return 'Make:'+data.carMake+'<br/> Model:'+data.carModel+'<br/> Variant:'+ data.carVariant;
							}},
							{ "data": "carId" },
							{ "data": "address" },
							{ "data": "inspectorId" },
							{"data": "customerId",
                                "width": "50px", "render": function (data, type, row, meta) {
                                return '<select class = "form-control" id= "" onchange="assign(this.id);"><option>Hello</option></select>';
							}},
						]
				    });
                },
				error: function (xhr, ajaxOptions, thrownError) {
					alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
					alert("responseText: "+xhr.responseText);
				}
            });
	   }

    </script>


    <script src="../../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="../../assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="../js/custom.min.js"></script>
    <!-- Magnific popup JavaScript -->
    <script src="../../assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <!-- This is data table -->
    <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../../assets/plugins/moment/moment.js"></script>
    <script src="../../assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	<!-- This is data table -->
    <!-- start - This is for export functionality only -->
    <script>
        $('#datefrom').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
        $('#dateto').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
		
		$(document).ready(function() {
        // $('#myTable').dataTable( {
			// "pageLength": 50
		// } );
		
		
		
		var table = $('#myTable').DataTable({
			'order': [1, 'asc'],
			"pageLength": 10
		});
		
		
	   
	   
	   
	   
   });
   
    </script>

</body>
</html>