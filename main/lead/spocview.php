<?php
ob_start();
session_start();
include_once("../database_config.php");
include_once("../functions.php");
date_default_timezone_set('Asia/Kolkata');
// if(!$_SESSION['id']){
   // header("location:index.php");
   // die;
// }
// $username= $_SESSION['username'];
// $uid = $_SESSION['id'];
// $uemail = $_SESSION['email'];
// $department = $_SESSION['department'];


// $sql_spoc = "SELECT `id`,`username` FROM `tbl_usermaster` WHERE `department` = 3";
// $query_spoc = mysqli_query($conn,$sql_spoc);

// $i = 0;
// while($row1 = mysqli_fetch_array($query_spoc)){
    // $spoc_names[$i] = $row1['username'];
    // $ids[$i] = $row1['id']; 
    // $i++;
// }

if(isset($_POST['btn-fetch']))
{
	
    
}
 else {
    $query = "SELECT customerId,customerName,phoneNumber,carMake,carModel,carManufacturingYear,priority,leadType,leadStatus,createdAt from TBL_LEAD_CAR_DATA where leadStatus = 51";
    $search_result = filterTable($query);
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>Bluejack - Lead Dashboard</title>
    <!-- Bootstrap Core CSS -->

    <link rel="preload" href="../modal.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../modal.css"></noscript>

    <link rel="preload" href="../../assets/plugins/bootstrap/css/bootstrap.min.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.min.css"></noscript> 
    <!-- Custom CSS -->
    <link rel="preload" href="../css/style.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../css/style.css"></noscript>

    <link rel="preload" href="../../assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../../assets/plugins/Magnific-Popup-master/dist/magnific-popup.css"></noscript>

    <!-- You can change the theme colors from here -->
    <link rel="preload" href="../css/colors/blue.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../css/colors/blue.css"></noscript>
   
    <link rel="preload" href="../../assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" as="style" onload="this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="../../assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"></noscript> 

   <script>
        !function(a){"use strict";var b=function(b,c,d){function j(a){if(e.body)return a();setTimeout(function(){j(a)})}function l(){f.addEventListener&&f.removeEventListener("load",l),f.media=d||"all"}var g,e=a.document,f=e.createElement("link");if(c)g=c;else{var h=(e.body||e.getElementsByTagName("head")[0]).childNodes;g=h[h.length-1]}var i=e.styleSheets;f.rel="stylesheet",f.href=b,f.media="only x",j(function(){g.parentNode.insertBefore(f,c?g:g.nextSibling)});var k=function(a){for(var b=f.href,c=i.length;c--;)if(i[c].href===b)return a();setTimeout(function(){k(a)})};return f.addEventListener&&f.addEventListener("load",l),f.onloadcssdefined=k,k(l),f};"undefined"!=typeof exports?exports.loadCSS=b:a.loadCSS=b}("undefined"!=typeof global?global:this);
        !function(a){if(a.loadCSS){var b=loadCSS.relpreload={};if(b.support=function(){try{return a.document.createElement("link").relList.supports("preload")}catch(a){return!1}},b.poly=function(){for(var b=a.document.getElementsByTagName("link"),c=0;c<b.length;c++){var d=b[c];"preload"===d.rel&&"style"===d.getAttribute("as")&&(a.loadCSS(d.href,d,d.getAttribute("media")),d.rel=null)}},!b.support()){b.poly();var c=a.setInterval(b.poly,300);a.addEventListener&&a.addEventListener("load",function(){b.poly(),a.clearInterval(c)}),a.attachEvent&&a.attachEvent("onload",function(){a.clearInterval(c)})}}}(this);
    </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="../../assets/images/Logo_Thumb_White.png" alt="homepage" class="light-logo" width="32px" height="31px" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="../../assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="../../assets/images/textWhite.png" class="light-logo" alt="homepage" width="98px" height="19px" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../../assets/images/users/1.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="../../assets/images/users/1.jpg" alt="user"></div>
                                            <div class="u-text">
                                                <h4><?php echo $username ?></h4>
                                                <p class="text-muted"><?php echo $uemail ?></p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Language -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flag-icon flag-icon-in"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ================================== 1============================ -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile" style="background: url(../../assets/images/background/user-info.jpg) no-repeat;">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="../../assets/images/users/profile.png" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php echo $username ?></a>
                        <div class="dropdown-menu animated flipInY"> <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a> <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a> <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                            <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                            <div class="dropdown-divider"></div> <a href="logout.php" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">BLUEJACK</li>
                        <?php 
							if($department == 3)
							{
								echo '

                                    <li>
                                        <a class="waves-effect waves-dark" aria-expanded="false" href="../lead/leaddashboard.php"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                                    </li>

                                    <li>
										<a class="waves-effect waves-dark" aria-expanded="false" href="../lead/leadassign.php"><i class="mdi mdi-gauge"></i><span class="hide-menu">Lead Assign </span></a>
									</li>
									';
							}
							
						
						?>
						
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                
                <!-- item--><a href="#" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Lead Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Lead Dashboard</li>
                        </ol>
                    </div>
                    
                </div>

                <div id="responsive-modal" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog" style="max-width: 80%;">
                        <div class="modal-content">
                            
                            <div class="modal-header">
                                <h3 class="modal-title">Details</h3>
                            </div>
                            
                            <div class="modal-body">

                                <form id = "lead_form">


                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divcustomer_name">
                                            <label for='customer_name'>Customer name</label>
                                            <input class = "form-control" id="customer_name" disabled>
                                        </div>    

                                        <div class='col-md-6' id="divphone_number">
                                            <label for="phone_number">Customer Phone Number</label>
                                            <input class = "form-control" id="phone_number" disabled>
                                        </div> 

                                    </div>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divcall_answered">
                                            <label for='call_answered'>Call Answered</label>
                                            <select id="call_answered" class="form-control select2" onchange="check_call_answered();">
                                                <option value="">--SELECT OPTION--</option>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>    

                                        <div class='col-md-6' id="divleadstatus">
                                            <label for='address'>Lead Status</label>
                                            <select class="form-control select2" style="width: 100%" name="leadstatus" id="leadstatus">
                                                <?php
                                                    $sql2="Select id as id, status as name from TBL_LEAD_STATUS where delid=0";
                                                    $result2=mysqli_query($conn, $sql2);
                                                    echo'<option value="">--SELECT OPTION--</option>';
                                                    while($row=mysqli_fetch_assoc($result2))
                                                    {   
                                                        $id = $row['id'];
                                                        $name = $row['name'];
                                                        if(substr($name,0,4) == 'SPOC'){
                                                            $status = substr($name,5);
                                                            echo "<option value='" . $id ."'>" . $status ."</option>";
                                                        }
                                                    } 
                                                ?>
                                                        
                                            </select>   
                                        </div>

                                    </div>

                                    <div class ="row">

                                        <div class="col-md-12" id="divcomments">
                                            <label for="comments">Comments</label>
                                            <input type="textarea" id="comments" class = "form-control" name="comments">
                                        </div>

                                    </div>

                                    <h4 class="box-title m-t-40 required">Lead Details</h4>
                                    <hr>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divcar_make">
                                            <label for='car_make'>Car Make</label>
                                            <select class="form-control select2" id="car_make" onchange="get_car_model();">
                                                <option value = "">--SELECT OPTION--</option>
                                                <?php 
                                                    $sql_car_make = "SELECT DISTINCT `carMake` FROM `TBL_CAR_TYPES`";
                                                    $query_car_make = mysqli_query($conn,$sql_car_make);
                                                    while($row = mysqli_fetch_array($query_car_make)){
                                                            echo "<option value='".$row['carMake']."'>".ucwords($row['carMake'])."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                        <div class='col-md-6' id="divcar_model">
                                            <label for='car_model'>Car Model</label>
                                            <select class="form-control select2" id="car_model" onchange="get_car_variant();">
                                                <option value = "">--SELECT OPTION--</option>
                                            
                                            </select>
                                        </div>   

                                    </div>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divcar_variant">
                                            <label for='car_variant'>Car Variant</label>
                                            <select class="form-control select2" id="car_variant">
                                                <option value="">--SELECT OPTION--</option>
                                            </select>
                                        </div> 

                                        <div class='col-md-6' id="divRegistration_Number">
                                            <label for='registration_number'>Car Registration Number</label>
                                            <input class="form-control select2" id="registration_number">
                                        </div> 

                                    </div>

                                    <div class = "row">
                                        
                                        <div class ="col-md-6" id="divmanufacturing_year">
                                            <label for="manufacturing_year">Manufacturing Year</label>
                                            <input id="manufacturing_year" class = "form-control" name="manufacturing_year">
                                        </div>
                                           

                                        <div class='col-md-6' id="divfuel_type">
                                            <label for="fuel_type">Fuel Type</label>
                                            <select class = "form-control select2" id="fuel_type">
                                                <option value="">--SELECT OPTION--</option>
                                                <option value="petrol">Petrol</option>
                                                <option value="diesel">Diesel</option>
                                                <option value="petrol/cng">Petrol/CNG</option>
                                                <option value="lpg">LPG</option>
                                            </select>
                                        </div> 
                                    </div>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divcolor">
                                            <label for='color'>Color</label>
                                            <input class = "form-control" id="color">
                                        </div>    

                                        <div class='col-md-6' id="divodometer_reading">
                                            <label for='odometer_reading'>Km Driven</label>
                                            <input class = "form-control" id="odometer_reading" type="number">
                                        </div>

                                    </div>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divcar_owner">
                                            <label for="car_owner">Car Owner</label>
                                            <input class = "form-control" id="car_owner" type="number">
                                        </div>     

                                        <div class='col-md-6' id="divexpected_price">
                                            <label for='expected_price'>Expected Price</label>
                                            <input class = "form-control" id="expected_price" type="number">
                                        </div>
                                        
                                    </div>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divpd_price">
                                            <label for='pd_price'>PD Price</label>
                                            <input class = "form-control" id="pd_price">
                                        </div>

                                        <div class='col-md-6' id="divseller">
                                            <label for='seller'>Seller</label>
                                            <select class = "form-control" id="seller">
                                                <option value="">--SELECT OPTION--</option>
                                                <option value="self">Self</option>
                                                <option value="other">Other</option>
                                            </select>
                                        </div>    

                                    </div>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divselling_reason">
                                            <label for='selling_reason'>Selling Reason</label>
                                            <input type="textarea" name="selling_reason" class="form-control" id="selling_reason">        
                                        </div>

                                        <div class='col-md-6' id="divaddress">
                                            <label for='address'>Address</label>
                                            <input name="address" class="form-control" id="address">        
                                        </div>    

                                    </div>

                                    <div class = "row">
                                        
                                        <div class='col-md-6' id="divaddress_type">
                                            <label for='address_type'>Address Type</label>
                                            <select name="address_type" class="form-control" id="address_type">    
                                                <option value="">--SELECT OPTION--</option>
                                                <option value="home">Home</option>
                                                <option value="office">Office</option>
                                            </select>    
                                        </div>
            
                                    </div>

                                    <div class = "row">
            
                                        <div class="col-md-6" id="divcustomer_id" hidden>
                                            <label for="customer_id">Customer Number</label>
                                            <input type="number" id="customer_id" class = "form-control" name="customer_id" >
                                        </div>

                                    </div>

                                    <div class="form-actions row">

                                        <div class = "col-md-6">
                                            <button type="button" class="btn btn-success" name="btn-save" style="margin-left: 80%; margin-top: 3%;" onclick="savebutton();"> <i class="fa fa-check"></i>Save</button>
                                        </div>

                                        <div class= "col-md-6">
                                            <button type="reset" class="btn btn-inverse" name="btn-cancel" style="margin-right: 80%; margin-top: 3%;" onclick="hideModal();">Cancel</button>
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Filters</h4>
                            </div>
                            <div class="card-body">
                                    <div class="form-body">
                                        <div class="row ">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label" name="assignedto">Lead Status</label>
                                                    <select class="form-control" style="width: 100%" name="leadstatus" id="leadstatus" onchange="filter_data();">
                                                        <?php
															$sql2="Select id as id, status as name from TBL_LEAD_STATUS where delid=0 and id > 100 and id < 120";
															$result2=mysqli_query($conn, $sql2);
															echo'<option value="0" disabled selected>---Select SPOC Name--- </option>';
															while($row=mysqli_fetch_assoc($result2))
															{
																$id = $row['id'];
																$name = $row['name'];
																echo "<option value='" . $id ."'>" . $name ."</option>";
															} 
															?>
														
                                                    </select>
                                                </div>
                                            </div>

                                        </div>  
 
                                        <!--</div> -->
                                            <!--/span-->
                                            
                                            <!--/span-->
                                        
                                        
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>


                <div class = "card">
                    <div class = "card-body">
                        <div class="table-responsive m-t-40">
										<form id="frm-example" action="" method="POST">
                                            <table id="myTable" class="table table-bordered table-striped">
                            
                                        <!--<div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">-->
                                                <thead>
                                                    <tr>
                                                        <th>Customer Name</th>
                                                        <th>Phone Number</th>
														<th>Car Make/Model/Year</th>
                                                        <th>Lead Status</th>
                                                        <th>Lead Date</th>
                                                        <th>Priority</th>
														<th>Lead Source</th>
														<th>Calls Attempted</th>
														<th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
													<?php while($row=mysqli_fetch_array($search_result))
											 {
												 echo "
												 <tr>
													<td>".  $row['customerName'] ."</td>
													<td>".  $row['phoneNumber'] ."</td>
													<td><b>Make:</b> ".  $row['carMake'] ." <br/> <b>Model:</b> ". $row['carModel'] ." <br/> <b>Year:</b> ". $row['carManufacturingYear'] ."</td>
													<td>".  $row['leadStatus'] ."</td>
													<td>".  $row['createdAt'] ."</td>
													<td>".  $row['priority'] ."</td>
													<td>".  $row['leadType'] ."</td>
													<td>".  2 ."</td>
													<td><i class ='mdi mdi-file-document icon-color' id = ".$row['customerId']." style='font-size:20px;' data-toggle='modal' data-target='.modalView' onclick='fetchDetails(this.id);'></td>
												</tr>
												 ";
											 }?>
                                                </tbody>
                                            </table>
										
                                    </div>
                                </div>
                            </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                

                <!-- Row -->
                <!-- Row -->
                <!-- Row -->
                <!-- Row -->
                <!-- Row -->
                  
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2017 Bluejack </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/plugins/jquery/jquery.min.js"></script>

    <script type="text/javascript">
        
        $(document).ready(function(){
            document.getElementById('divcar_make').style.display = "none";
            document.getElementById('divcar_model').style.display = "none";
            document.getElementById('divcar_variant').style.display = "none";
            document.getElementById('divmanufacturing_year').style.display = "none";
            document.getElementById('divfuel_type').style.display = "none";
            document.getElementById('divcolor').style.display = "none";
            document.getElementById('divodometer_reading').style.display = "none";
            document.getElementById('divcar_owner').style.display = "none";
            document.getElementById('divexpected_price').style.display = "none";
            document.getElementById('divpd_price').style.display = "none";
            document.getElementById('divseller').style.display = "none";
            document.getElementById('divselling_reason').style.display = "none";
            document.getElementById('divaddress').style.display = "none";
            document.getElementById('divaddress_type').style.display = "none";
            document.getElementById('divRegistration_Number').style.display = "none";
        });

        function showMessage() {
                
            alert("Clearing set filters.");
            $('#datefrom').val('');
            $('#dateto').val('');
            $("#filterstat").val("");
            var table = $('#myTable').DataTable();
               table.clear().draw();
        }

        

        function fetchDetails(a){
            var customer_id = a;

            $.ajax({
                url: 'fetchDetails.php',
                type: 'POST',
                data: {customer_id:customer_id},
                dataType: 'json',
                
                success:function(data){
                    document.getElementById('customer_name').value = data.customer_name;
                    document.getElementById('phone_number').value = data.phone_number;   
                    document.getElementById('customer_id').value = data.customer_id;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                    alert("responseText: "+xhr.responseText);
                }
            });

        }
		
        function hideModal(){
            $('#responsive-modal').modal('hide');
            $('.modal-backdrop').remove();
        }

        function check_call_answered(){
            var call_answered = document.getElementById('call_answered').value;
            if(call_answered == 'yes'){
                document.getElementById('divcar_make').style.display = "block";
                document.getElementById('divcar_model').style.display = "block";
                document.getElementById('divcar_variant').style.display = "block";
                document.getElementById('divmanufacturing_year').style.display = "block";
                document.getElementById('divfuel_type').style.display = "block";
                document.getElementById('divcolor').style.display = "block";
                document.getElementById('divodometer_reading').style.display = "block";
                document.getElementById('divcar_owner').style.display = "block";
                document.getElementById('divexpected_price').style.display = "block";
                document.getElementById('divpd_price').style.display = "block";
                document.getElementById('divseller').style.display = "block";
                document.getElementById('divselling_reason').style.display = "block";
                document.getElementById('divaddress').style.display = "block";
                document.getElementById('divaddress_type').style.display = "block";
                document.getElementById('divRegistration_Number').style.display = "block";
            }else{
                document.getElementById('divcar_make').style.display = "none";
                document.getElementById('divcar_model').style.display = "none";
                document.getElementById('divcar_variant').style.display = "none";
                document.getElementById('divmanufacturing_year').style.display = "none";
                document.getElementById('divfuel_type').style.display = "none";
                document.getElementById('divcolor').style.display = "none";
                document.getElementById('divodometer_reading').style.display = "none";
                document.getElementById('divcar_owner').style.display = "none";
                document.getElementById('divexpected_price').style.display = "none";
                document.getElementById('divpd_price').style.display = "none";
                document.getElementById('divseller').style.display = "none";
                document.getElementById('divselling_reason').style.display = "none";
                document.getElementById('divaddress').style.display = "none";
                document.getElementById('divaddress_type').style.display = "none";
                document.getElementById('divRegistration_Number').style.display = "none";
            }
        }

        function savebutton(){
            
            var customer_name = document.getElementById('customer_name').value;
            var phone_number = document.getElementById('phone_number').value;
            var call_answered = document.getElementById('call_answered').value;
            var leadstatus = document.getElementById('leadstatus').value;

            if(call_answered == 'yes'){
                var car_make =  document.getElementById('car_make').value;
                var car_model =  document.getElementById('car_model').value;
                var car_variant =  document.getElementById('car_variant').value;
                var manufacturing_year =  document.getElementById('manufacturing_year').value;
                var fuel_type =  document.getElementById('fuel_type').value;
                var color =  document.getElementById('color').value;
                var odometer_reading =  document.getElementById('odometer_reading').value;
                var car_owner =  document.getElementById('car_owner').value;
                var expected_price =  document.getElementById('expected_price').value;
                var pd_price =  document.getElementById('pd_price').value;
                var seller =  document.getElementById('seller').value;
                var selling_reason =  document.getElementById('selling_reason').value;
                var address =  document.getElementById('address').value;
                var address_type =  document.getElementById('address_type').value;
                var comments = document.getElementById('comments').value;
                var customer_id = document.getElementById('customer_id').value;
                var registration_number = document.getElementById('registration_number').value;

                $.ajax({
                    url: 'save_spoc_view_data.php',
                    type: 'POST',
                    data: {customer_id:customer_id,customer_name:customer_name,phone_number:phone_number,leadstatus:leadstatus,call_answered:call_answered,car_make:car_make,car_model:car_model,car_variant:car_variant,manufacturing_year:manufacturing_year,fuel_type:fuel_type,color:color,odometer_reading:odometer_reading,car_owner:car_owner,expected_price:expected_price,pd_price:pd_price,seller:seller,selling_reason:selling_reason,address:address,address_type:address_type,comments:comments,registration_number:registration_number},
                    dataType: 'text',
                    
                    success:function(data){
                        console.log(data);
                        alert(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                        alert("responseText: "+xhr.responseText);
                    }
                });
            }else if(call_answered == 'no'){
                $.ajax({
                    url: 'save_spoc_view_data.php',
                    type: 'POST',
                    data: {customer_id:customer_id,customer_name:customer_name,phone_number:phone_number,leadstatus:leadstatus,call_answered},
                    dataType: 'text',
                    
                    success:function(data){
                        alert(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                        alert("responseText: "+xhr.responseText);
                    }
                });
            }
        } 

		function countCheck() {
			var assign = document.getElementById('spocname').value
        $("input:checkbox[class=checkid]").each(function () {
			
			if($(this).is(":checked"))
			{
				var id = $(this).val();
				alert(id);
					$.ajax({
					url: 'assignspoc_ajax.php',
					type: 'POST',
					data: {id:id,assign:assign},
					dataType: 'text',
					
					//window.alert(id2);
					success:function(data){
						alert('Successfull');  
					},
					error: function (xhr, ajaxOptions, thrownError) {
						//alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
						//alert("responseText: "+xhr.responseText);
					}
				});
				
			}
            //alert("Id: " + $(this).attr("id") + " Value: " + $(this).val() + " Checked: " + $(this).is(":checked"));
        });
            // var items = document.getElementsByClassName('checkid');
			// for (var i = 0; i < items.length; i++)
				// alert(items[i].name);   
            
        }
		
        $('#datefrom').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
        $('#dateto').bootstrapMaterialDatePicker({ weekStart : 0, time: false });

        function get_car_model(){

            var car_make = document.getElementById('car_make').value;
                
            $.ajax({
                url: 'get_car_model.php',
                type: 'POST',
                data: {car_make:car_make},
                dataType: 'HTML',
                
                success:function(data){
                    document.getElementById('car_model').innerHTML = data;   
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                    alert("responseText: "+xhr.responseText);
                }
            });   
        }

        function get_car_variant(){
            
            var car_model = document.getElementById('car_model').value;
            var car_make = document.getElementById('car_make').value;

            $.ajax({
                url: 'get_car_variant.php',
                type: 'POST',
                data: {car_model:car_model,car_make:car_make},
                dataType: 'HTML',
                
                success:function(data){
                    document.getElementById('car_variant').innerHTML = data;   
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                    alert("responseText: "+xhr.responseText);
                }
            }); 
        }

        function assign(a){
            var id = a.substr(3);
            var spoc = document.getElementById(a).value;
            
            $.ajax({
                url: 'assign_lead_to.php',
                type: 'POST',
                data: {id:id,spoc:spoc},
                dataType: 'text',
                
                //window.alert(id2);
                success:function(data){
                    alert('Successfull');
                    document.getElementById(id).style.display = "none";   
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
                    alert("responseText: "+xhr.responseText);
                }
            });

        }
		
		function filter_data()
	   {
	        var leadstatus = document.getElementById("leadstatus").value;
          
		   
		    var table = $('#myTable').DataTable();
		    table.clear().draw();
		    table.destroy();
		   
			$.ajax({
                url: 'filterdata_3.php',
                type: 'POST',
                data: {leadstatus:leadstatus},
                dataType: 'json',
				
                success:function(result){
					$('#myTable').DataTable( {
						"pageLength": 10,
						"destroy": true,
						"language": {
								"zeroRecords": "No Data Found",
								"infoEmpty": "No Data Found",
							},
					"ajax": "objects2.txt",
						"columns": [
							{ "data": "customerName" },
							{ "data": "phoneNumber" },
							{ "data": "carMake",},
							{ "data": "leadStatus" },
							{ "data": "createdAt" },
							{ "data": "priority" },
							{ "data": "leadType" },
							{ "data": "callAttempted" },
						]
				    });
                },
				error: function (xhr, ajaxOptions, thrownError) {
					alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
					alert("responseText: "+xhr.responseText);
				}
            });
	   }

    </script>


    <script src="../../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="../../assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="../js/custom.min.js"></script>
    <!-- Magnific popup JavaScript -->
    <script src="../../assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <!-- This is data table -->
    <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../../assets/plugins/moment/moment.js"></script>
    <script src="../../assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	<!-- This is data table -->
    <!-- start - This is for export functionality only -->
    <script>
        $('#datefrom').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
        $('#dateto').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
		
		$(document).ready(function() {
        // $('#myTable').dataTable( {
			// "pageLength": 50
		// } );
		
		
		
		var table = $('#myTable').DataTable({
			'order': [1, 'asc'],
			"pageLength": 10
		});
		
		
	   
	   
	   
	   
   });
   
    </script>

</body>
</html>