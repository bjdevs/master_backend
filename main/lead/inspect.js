$(".inspectCheck").click(function(){

	var id=this.id;
	var thisId="#"+id;
	var lastChar = id.substring(6);
	var iconId="#iconstatus"+lastChar;
	//alert(lastChar);
	$.ajax({
	    type: "GET",
	    url: "changeStatus.php",
	    data: {id:lastChar},
	    cache: false,
	    dataType: 'json',
	    success: function(data) {
	    	//var op=parseInt(data.status);
	        //alert(data.status);
	        if(data.status==1){
		        $(thisId).removeClass("btn-success");
		    	$(iconId).removeClass("mdi-check");
		    	$(thisId).addClass("btn-danger");
		    	$(iconId).addClass("mdi-close");
				}
			else if(data.status==2){
				$(thisId).addClass("btn-success");
		    	$(iconId).addClass("mdi-check");
		    	$(thisId).removeClass("btn-danger");
		    	$(iconId).removeClass("mdi-close");
			}
	        
	    },
	    error: function(err) {
	    	alert(err);
	    }
    });
});



function filterStatus(buttonid){
	//alert(buttonid);
	var thisId="#"+buttonid;
	var lastChar = buttonid.substring(6);
	var iconId="#iconstatus"+lastChar;
	//alert(lastChar);

		$.ajax({
	    type: "GET",
	    url: "changeStatus.php",
	    data: {id:lastChar},
	    cache: false,
	    dataType: 'json',
	    success: function(data) {
	    	//var op=parseInt(data.status);
	        //alert(data.status);
	        if(data.status==1){
		        $(thisId).removeClass("btn-success");
		    	$(iconId).removeClass("mdi-check");
		    	$(thisId).addClass("btn-danger");
		    	$(iconId).addClass("mdi-close");
				}
			else if(data.status==2){
				$(thisId).addClass("btn-success");
		    	$(iconId).addClass("mdi-check");
		    	$(thisId).removeClass("btn-danger");
		    	$(iconId).removeClass("mdi-close");
			}
	        
	    },
	    error: function(err) {
	    	alert(err);
	    }
    });
	
}