<?php
include_once("../database_config.php");
$username='Indranil';

$filename=$_FILES["file"]["tmp_name"];
if($_FILES["file"]["size"] > 0)
{
    $flag = true;
    $file = fopen($filename, "r");
    while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE){
        //It wiil insert a row to our subject table from our csv file`
        if($flag){
            $flag = false;
            continue;
        }

        if($emapData[17] == 'TRUE'){
            $post_volume_check = 1;
        }else{
            $post_volume_check = 0;
        }

        if($emapData[18] == 'TRUE'){
            $seller_title_check = 1;
        }else{
            $seller_title_check = 0;
        }

        if($emapData[19] == 'TRUE'){
            $post_title_check = 1;
        }else{
            $post_title_check = 0;
        }

        if($emapData[20] == 'TRUE'){
            $post_body_check = 1;
        }else{
            $post_body_check = 0;
        }

        if($emapData[21] == 'TRUE'){
            $dealer_final_check = 1;
        }else{
            $dealer_final_check = 0;
        }

        if($emapData[22] == 'TRUE'){
            $luxury_check = 1;
        }else{
            $luxury_check = 0;
        }
		
		$sellerName = mysqli_real_escape_string($conn,$emapData[0]);
		$phoneNumber = mysqli_real_escape_string($conn,$emapData[1]);
		$adUrl = mysqli_real_escape_string($conn,$emapData[2]);
		$carRegistrationNumber = mysqli_real_escape_string($conn,$emapData[3]);
		$carBrand = mysqli_real_escape_string($conn,$emapData[4]);
		$carModel = mysqli_real_escape_string($conn,$emapData[5]);
		$carManufacturingYear = mysqli_real_escape_string($conn,$emapData[6]);
		$carFuel = mysqli_real_escape_string($conn,$emapData[7]);
		$odometerReading = mysqli_real_escape_string($conn,$emapData[23]);
		$adTitle =  mysqli_real_escape_string($conn,$emapData[9]);
		$adID = mysqli_real_escape_string($conn,$emapData[10]);
		$adLocation = mysqli_real_escape_string($conn,$emapData[11]);
		$expectedPrice = mysqli_real_escape_string($conn,$emapData[12]);
		$adContent = mysqli_real_escape_string($conn,$emapData[13]);
		$adAddedOn = mysqli_real_escape_string($conn,$emapData[14]);
		$postVolumeCheck = mysqli_real_escape_string($conn,$emapData[15]);
		$sellerTitleCheck = mysqli_real_escape_string($conn,$emapData[16]);
		$postTitleCheck = mysqli_real_escape_string($conn,$emapData[17]);
		$postBodyCheck = mysqli_real_escape_string($conn,$emapData[18]);
		$dealerFinalCheck = mysqli_real_escape_string($conn,$emapData[19]);
		$luxuryCheck = mysqli_real_escape_string($conn,$emapData[20]);
		$registrationState = mysqli_real_escape_string($conn,$emapData[21]);
		$keyYear = mysqli_real_escape_string($conn,$emapData[22]);
		$keyOdometer = mysqli_real_escape_string($conn,$emapData[24]);
		$keyFinal = mysqli_real_escape_string($conn,$emapData[25]);
		$customerPriority = mysqli_real_escape_string($conn,$emapData[26]);
		$leadId = 2;
		$leadType = 'OLX';
		$carVariant = '';
		$carColor = '';
		
		
        $sql_upload = "INSERT INTO `TBL_LEAD_OLX_SCRAPPER_DATA`(`customerName`,`phoneNumber`, `adURL`, `carRegistrationNumber`, `carBrand`, `carModel`, `carManufacturingYear`, `carFuel`, `odometerReading`, 
		`adTitle`, `adID`, `adLocation`, `expectedPrice`, `adContent`, `adAddedOn`, `postVolumeCheck`, `sellerTitleCheck`, `postTitleCheck`, `postBodyCheck`, `dealerFinalCheck`, `luxuryCheck`, 
		`registrationState`, `keyYear`, `keyOdometer`, `keyFinal`, `customerPriority`,`uploadedBy`) VALUES ('$seller_name','$phoneNumber','$adUrl','$carRegistrationNumber',
		'$carBrand','$carModel','$carManufacturingYear','$carFuel','$odometerReading','$adTitle','$adID','$adLocation','$expectedPrice','$adContent','$adAddedOn','$postVolumeCheck','$sellerTitleCheck','$postTitleCheck','$postBodyCheck','$dealerFinalCheck','$luxuryCheck','$registrationState','$keyYear',
		'$keyOdometer','$keyFinal','$customerPriority','$username')";

		
		$sql_upload_lead_master = "INSERT INTO `TBL_LEAD_CAR_DATA`(`customerName`,`phoneNumber`, `leadType`,`leadId`,`URL`, `carRegistrationNumber`, `carMake`, `carModel`,`carvariant`, `carManufacturingYear`, `carFuelType`, `carColor`,`carKmDriven`, `Priority`) VALUES ('$sellerName','$phoneNumber','$leadType','$leadId','$adUrl','$carRegistrationNumber','$carBrand',
		'$carModel','$carVariant','$carManufacturingYear','$carFuel','$carColor','$odometerReading','$customerPriority')";

        //we are using mysql_query function. it returns a resource on true else False on error
        $result = mysqli_query($conn,$sql_upload);
        $result1 = mysqli_query($conn,$sql_upload_lead_master);
        if(! $result )
        {
            echo "<script type=\"text/javascript\">
            alert(\"Invalid File:Please Upload CSV File.\");
            window.location = \"index.php\"
            </script>";
        }

    }

    fclose($file);
     //throws a message if data successfully imported to mysql database from excel file
}
else{
    echo "File size is less than 0";
}

?>