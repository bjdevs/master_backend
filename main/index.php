<?php
ob_start();
session_start();
include_once("database_config.php");
date_default_timezone_set('Asia/Kolkata');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Bluejack - Login Page</title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="../assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar"  style="background-image:url(../assets/images/background/login-register.jpg);">
  <div class="login-box card">
    <div class="card-body">
      <form class="form-horizontal form-material" id="loginform" method="POST">
        <a href="javascript:void(0)" class="text-center db"><img src="../assets/images/Logo_Thumb_Blue.png" alt="Home" width="40px" height="50px" /><br/><img src="../assets/images/textBlue.png" alt="Home" height="50px" /></a>  
        
        <div class="form-group m-t-40">
		  <div class="col-xs-12">
			<h5 class="m-t-30">Select Department</h5>
            <select class="select2" style="width: 100%" name="deptname" id="deptname">
                <option value="0">---Select Department---</option>
				<?php
				$sql = "select id,departmentname from `tbl_deptmaster` where delid=0";
				$res = mysqli_query($conn, $sql);
				if(mysqli_num_rows($res) > 0) {
					while($row = mysqli_fetch_object($res)) {
						echo "<option value='".$row->id."'>".$row->departmentname."</option>";
						}
					}
				?>                                    
            </select>
          </div>
        </div>
		<div class="form-group">
			<div class="col-xs-12">
				<input class="form-control" type="text" required="" id="email" name="email" placeholder="Email">
			</div>
		</div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" id="password" name="password" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Remember me </label>
            </div>
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="btn-login">Log In</button>
          </div>
        </div>
        
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <!--<p>Don't have an account? <a href="register2.html" class="text-primary m-l-5"><b>Sign Up</b></a></p> -->
          </div>
        </div>
      </form>
      <form class="form-horizontal" id="recoverform" method="POST">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
        </div>
      </form>
	  <?php
										if(isset($_POST['btn-login']))
										{
											$deptname=$_POST['deptname'];
											$username=$_POST['email'];
											$userpass=$_POST['password'];
											
											if($deptname > 0)
											{
											
											$sql = "Select * from  tbl_usermaster where email = '$username' AND userpwd = '$userpass' and department=$deptname and delid=0 and approved=0";
											echo $sql;
											$run_user = mysqli_query($conn, $sql);
											$row=mysqli_fetch_assoc($run_user);
											$checksql = mysqli_num_rows($run_user);
											if($checksql > 0 )
											{
												$_SESSION['username']=$row['username'];
												$_SESSION['id'] = $row['id'];
												$_SESSION['email'] = $row['email'];
												$_SESSION['department'] = $row['department'];
												$id = $row['id'];
												$department = $row['department'];
												$approved = $row['approved'];


                                                $active_pages = array();
                                                $sql1 = "SELECT pageid FROM tbl_settings WHERE `userid` = '$id' AND `active` = 1";
                                                $query1 = mysqli_query($conn,$sql1);

                                                while($row1 = mysqli_fetch_array($query1)) {
                                                    $a = $row1['pageid'];
                                                    array_push($active_pages,$a);
                                                }   

                                                $_SESSION['allowed_pages'] = $active_pages;
												$lastlogintime = $row['tmplogintime'];
												$datetime = date('d-m-Y H:i:s');
												if($approved != 1)
												{
													//session_start();
													$sql2 = "update tbl_usermaster set tmplogintime = '$datetime' where id=$id";
													//echo $sql2;
													$qur = mysqli_query($conn, $sql2);
													if($qur)
													{
														if($department != 3 && $department != 4)
														{
															header("Location: dashboard.php");
														}
                                                        else if($department == 4){
                                                            header("Location: lead/dataupload.php");   
                                                        }
														else
														{
															header("Location: lead/leaddashboard.php");
														}
													}
													else
													{
														//header("Location: search.php");
														echo "Some Error Occured Try Again!!!";
													}
												}
												else
												{
													echo "you email id is not approved. Kindly contact Admin";
												}
												
											}
											else
											{
												echo "email or password is not correct, try again";
											}
											

												mysqli_close($conn);
												ob_end_flush();
											}
											else
											{
												echo "<script type='text/javascript'>alert('Please Select Department First');</script>";
											}
 
										}	
					
				?>
    </div>
  </div>
</section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="../assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
	<script src="../assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="indexjs.js"></script>
</body>


<!-- Mirrored from wrappixel.com/demos/admin-templates/material-pro/material/pages-login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Dec 2017 18:12:54 GMT -->
</html>