<?php
/*This function returns cardetails such as carnumber, carmake,carmodel,carvariant using corresponding carid values provided by calling the function.php file and invoking the funtion.*/
require_once("database_config.php");
include('vendor/autoload.php');
use PHPMailer\PHPMailer\PHPMailer;

function fetchdetails($id) {
	global $conn;
	$sql = "SELECT `carnumber`,`carmake`,`carmodel`,`carvariant` from `finallead` WHERE `carid`=$id";
	$query = mysqli_query($conn,$sql);
	$row=mysqli_fetch_assoc($query);
	$carnumber = $row['carnumber'];
	$carmake = $row['carmake'];
	$carmodel = $row['carmodel'];
	$carvariant = $row['carvariant'];

	$json = array('carnumber'=>$carnumber,'carmake'=>$carmake,'carmodel'=>$carmodel,'carvariant'=>$carvariant);
	echo json_encode($json,true);

}

function car_sold($id) {
	global $conn;
	$sql = "SELECT `carnumber` from `inspectionreport` WHERE `carid`=$id";
	$query = mysqli_query($conn,$sql);
	$row=mysqli_fetch_assoc($query);
	$carnumber = $row['carnumber'];

	$json = array('carnumber'=>$carnumber);
	echo json_encode($json,true);

}

function filterTable($query)
{
	global $conn;
    $filter_Result = mysqli_query($conn, $query);
    return $filter_Result;
}

function purchase_cancellation($id){
	global $conn;
	
	$sql = "SELECT `car_number`,`car_make`,`car_model`,`car_variant`,`lead_source`,`other_lead_source`,`seller_name`,`seller_phone`,`seller_address`,`vahan_data`,`account_details`,`buying_price`,`service_record`,`commission_amount`,`additional_charges`,`description_of_add_charges`,`channel`,`rsd_amount`,`transaction_type` from `carbought` WHERE `carid`=$id";
	
	$query = mysqli_query($conn,$sql);
	$row = mysqli_fetch_assoc($query);

	$carnumber = $row['car_number'];
	$carmake = $row['car_make'];
	$carmodel = $row['car_model'];
	$carvariant = $row['car_variant'];
	$lead_source = $row['lead_source'];
	$seller_phone = $row['seller_phone'];
	$seller_name = $row['seller_name'];
	$seller_address = $row['seller_address'];
	$vahan_data = $row['vahan_data'];
	$account_details = $row['account_details'];
	$buying_price = $row['buying_price'];
	$service_record = $row['service_record'];
	$commission_amount = $row['commission_amount'];
	$additional_charges = $row['additional_charges'];
	$description_of_add_charges = $row['description_of_add_charges'];
	$channel = $row['channel'];
	$rsd_amount = $row['rsd_amount'];
	$transaction_type = $row['transaction_type'];
	$other_lead_source = $row['other_lead_source'];
	
	if($other_lead_source != ""){
		$json = array('carnumber'=>$carnumber,'carmake'=>$carmake,'carmodel'=>$carmodel,'carvariant'=>$carvariant,'lead_source'=>$lead_source,'other_lead_source'=>$other_lead_source,'seller_name'=>$seller_name,'seller_phone'=>$seller_phone,'seller_address'=>$seller_address,'vahan_data'=>$vahan_data,'account_details'=>$account_details,'buying_price'=>$buying_price,'service_record'=>$service_record,'commission_amount'=>$commission_amount,'additional_charges'=>$additional_charges,'description_of_add_charges'=>$description_of_add_charges,'channel'=>$channel,'rsd_amount'=>$rsd_amount,'transaction_type'=>$transaction_type);
	}
	else{
		$json = array('carnumber'=>$carnumber,'carmake'=>$carmake,'carmodel'=>$carmodel,'carvariant'=>$carvariant,'lead_source'=>$lead_source,'seller_name'=>$seller_name,'seller_phone'=>$seller_phone,'seller_address'=>$seller_address,'vahan_data'=>$vahan_data,'account_details'=>$account_details,'buying_price'=>$buying_price,'service_record'=>$service_record,'commission_amount'=>$commission_amount,'additional_charges'=>$additional_charges,'description_of_add_charges'=>$description_of_add_charges,'channel'=>$channel,'rsd_amount'=>$rsd_amount,'transaction_type'=>$transaction_type);
	}

	echo json_encode($json,true);

}

function sale_cancellation($id){
	global $conn;
	
	$sql = "SELECT `sales_manager`,`carnumber`,`brand`,`carmodel`,`variant`,`dealerid`,`dealership_name`,`amountpaid`,`sold_amount`,`cf_amount`,`payment_details`,`delivery_date`,`token_amount`,`payment_receiver`,`prev_token_forfeit`,`bid_entered`,`tt`,`engine_displacement`,`car_length`,`payment_mode`,`total_deal_value` from `car_sold_form` WHERE `carid`=$id";
	
	$query = mysqli_query($conn,$sql);
	$row = mysqli_fetch_assoc($query);

	$sales_manager = $row['sales_manager'];
	$carnumber = $row['carnumber'];
	$brand = $row['brand'];
	$carmodel = $row['carmodel'];
	$variant = $row['variant'];
	$dealerid = $row['dealerid'];
	$dealership_name = $row['dealership_name'];
	$amountpaid = $row['amountpaid'];
	$sold_amount = $row['sold_amount'];
	$cf_amount = $row['cf_amount'];
	$payment_details = $row['payment_details'];
	$delivery_date = $row['delivery_date'];
	$token_amount = $row['token_amount'];
	$payment_receiver = $row['payment_receiver'];
	$prev_token_forfeit = $row['prev_token_forfeit'];
	$bid_entered = $row['bid_entered'];
	$tt = $row['tt'];
	$engine_displacement = $row['engine_displacement'];
	$car_length = $row['car_length'];
	$paymentmode = $row['payment_mode'];
	$totaldealvalue = $row['total_deal_value'];
	
	$json = array('salesmanager'=>$sales_manager,'carnumber'=>$carnumber,'brand'=>$brand,'carmodel'=>$carmodel,'variant'=>$variant,'dealerid'=>$dealerid,'dealershipname'=>$dealership_name,'amountpaid'=>$amountpaid,'soldamount'=>$sold_amount,'cfamount'=>$cf_amount,'paymentdetails'=>$payment_details,'deliverydate'=>$delivery_date,'tokenamount'=>$token_amount,'paymentreceiver'=>$payment_receiver,'prevtokenforfeit'=>$prev_token_forfeit,'bidentered'=>$bid_entered,'tt'=>$tt,'enginedisplacement'=>$engine_displacement,'carlength'=>$car_length,'paymentmode'=>$paymentmode,'totaldealvalue'=>$totaldealvalue);
	echo json_encode($json,true);
}

function email_carbought_report($email_address,$spoc,$channel,$carid,$car_registration,$carmake,$carmodel,$leadsource,$sellername,$sellerphone_number,$seller_address,$vahaandata,$transactiontype,$buyingprice,$commission_amount,$rsd_amount,$service_record) {

	$sentFrom = 'prashantsinghbana673@gmail.com';
	$senderPassword = 'bububana2205';

    $body = '
    <table style="border-radius:2px;margin:20px 0 25px;min-width:400px;border:1px solid #eee" cellspacing="0" cellpadding="10" border="0">
		<tbody>
			
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Email address</td>
				<td style="border-bottom:1px solid #eee"><a href="mailto:'.$email_address.'" target="_blank">'.$email_address.'</a></td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">SPOC</td>
				<td style="border-bottom:1px solid #eee">'.$spoc.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Channel</td>
				<td style="border-bottom:1px solid #eee">'.$channel.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">CarID</td>
				<td style="border-bottom:1px solid #eee">'.$carid.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Registration Number</td>
				<td style="border-bottom:1px solid #eee">'.$car_registration.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Make</td>
				<td style="border-bottom:1px solid #eee">'.$carmake.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Model</td>
				<td style="border-bottom:1px solid #eee">'.$carmodel.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Lead Source</td>
				<td style="border-bottom:1px solid #eee">'.$leadsource.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Seller Name</td>
				<td style="border-bottom:1px solid #eee">'.$sellername.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Seller Phone Number</td>
				<td style="border-bottom:1px solid #eee">'.$sellerphone_number.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Seller Address</td>
				<td style="border-bottom:1px solid #eee">'.$seller_address.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">VAHAN Data</td>
				<td style="border-bottom:1px solid #eee">'.$vahaandata.'</td>
			</tr>
				
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Transaction Type</td>
				<td style="border-bottom:1px solid #eee">'.$transactiontype.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Buying Price</td>
				<td style="border-bottom:1px solid #eee">'.$buyingprice.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Commission Amount</td>
				<td style="border-bottom:1px solid #eee">'.$commission_amount.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">RSD Amount</td>
				<td style="border-bottom:1px solid #eee">'.$rsd_amount.'</td>
			</tr>
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Service Record</td>
				<td style="border-bottom:1px solid #eee">'.$service_record.'</td>
			</tr>
		</tbody>
	</table>';
	
	$recipient = 'prashant@bluejack.in';
	$recipient1 = 'divyanshu@bluejack.in';
    $mail = new PHPMailer();
	$mail->Host= "smtp.gmail.com";
	$mail->isSMTP();
	//	$mail->SMTPDebug  = 2;
	$mail->Subject = "Car Bought Report | ".$carid."|".$carmake."|".$carmodel."|".$car_registration;
	$mail->IsHTML(true);
	$mail->SMTPAuth = true;
	$mail->Username = $sentFrom;
	$mail->Password = $senderPassword;
	$mail->SMTPSecure ='tls';
	$mail->Port = 587;
	$mail->Subject = "Car Bought Report | ".$carid."|".$carmake."|".$carmodel."|".$car_registration;
	$mail->Body = $body;
	$mail-> setFrom($sentFrom,'Bluejack');
	$mail -> addAddress($recipient,'Prashant');
	$mail -> addAddress($recipient1,'Divyanshu');

	if($mail->send()) {
		echo "Mail Sent";
	}
	else {
		echo "Not Working";
	}
	
}


function email_carsold_report($email_address,$salesmanager,$carid,$car_registration,$carmake,$carmodel,$variant,$dealerid,$dealershipname,$tokenstatus,$soldamount,$cfamount,$modeofpayment,$paymentdetails,$totaldealvalue,$deliverydate,$tokenamount,$paymentreceiver,$prevtokenforfeit,$bidentered,$tt,$enginedisplacement,$carlength) {

	$sentFrom = 'prashantsinghbana673@gmail.com';
	$senderPassword = 'bububana2205';
		
    $body = '
    Hi All,

    The following car has been sold : 

    <table style="border-radius:2px;margin:20px 0 25px;min-width:400px;border:1px solid #eee" cellspacing="0" cellpadding="10" border="0">
		<tbody>
			
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Email address</td>
				<td style="border-bottom:1px solid #eee"><a href="mailto:'.$email_address.'" target="_blank">'.$email_address.'</a></td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Sales Manager</td>
				<td style="border-bottom:1px solid #eee">'.$salesmanager.'</td>
			</tr>			

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">CarID</td>
				<td style="border-bottom:1px solid #eee">'.$carid.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Registration Number</td>
				<td style="border-bottom:1px solid #eee">'.$car_registration.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Make, Model and Variant</td>
				<td style="border-bottom:1px solid #eee">'.$carmake.' '.$carmodel.' '.$variant.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Dealer Id</td>
				<td style="border-bottom:1px solid #eee">'.$dealerid.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Dealership Name</td>
				<td style="border-bottom:1px solid #eee">'.$dealershipname.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Token / Full Amount</td>
				<td style="border-bottom:1px solid #eee">'.$tokenstatus.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Sold Amount</td>
				<td style="border-bottom:1px solid #eee">'.$soldamount.'</td>
			</tr>
				
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">CF Amount</td>
				<td style="border-bottom:1px solid #eee">'.$cfamount.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Mode Of Payment</td>
				<td style="border-bottom:1px solid #eee">'.$modeofpayment.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Payment Details</td>
				<td style="border-bottom:1px solid #eee">'.$paymentdetails.'</td>
			</tr>

			<tr style="background:#f9f9f9">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Total Deal Value(Including CF Amount & Prev Token Forfeit(if any))</td>
				<td style="border-bottom:1px solid #eee">'.$totaldealvalue.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Delivery Date</td>
				<td style="border-bottom:1px solid #eee">'.$deliverydate.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Token Amount</td>
				<td style="border-bottom:1px solid #eee">'.$tokenamount.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Payment Receiver</td>
				<td style="border-bottom:1px solid #eee">'.$paymentreceiver.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Previous Token Forfeit(if any)</td>
				<td style="border-bottom:1px solid #eee">'.$prevtokenforfeit.'</td>
			</tr>

			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Bid Entered</td>
				<td style="border-bottom:1px solid #eee">'.$bidentered.'</td>
			</tr>
			
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">TT</td>
				<td style="border-bottom:1px solid #eee">'.$tt.'</td>
			</tr>
			
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Engine Displacement</td>
				<td style="border-bottom:1px solid #eee">'.$enginedisplacement.'</td>
			</tr>
			
			<tr style="background:#fbfbfb">
				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Length</td>
				<td style="border-bottom:1px solid #eee">'.$carlength.'</td>
			</tr>

		</tbody>
	</table>';

	$recipient = 'prashant@bluejack.in';
	$recipient1 = 'divyanshu@bluejack.in';
    $mail = new PHPMailer();
	$mail->Host= "smtp.gmail.com";
	$mail->isSMTP();
	//	$mail->SMTPDebug  = 2;
	$mail->Subject = "Car Sold Report | ".$carid."|".$carmake." ".$carmodel."|".$car_registration;
	$mail->IsHTML(true);
	$mail->SMTPAuth = true;
	$mail->Username = $sentFrom;
	$mail->Password = $senderPassword;
	$mail->SMTPSecure ='tls';
	$mail->Port = 587;
	$mail->Subject = "Car Sold Report | ".$carid."|".$carmake." ".$carmodel."|".$car_registration;
	$mail->Body = $body;
	$mail-> setFrom($sentFrom,'Bluejack');
	$mail -> addAddress($recipient,'Prashant');
	$mail -> addAddress($recipient1,'Divyanshu');

	if($mail->send()){
		echo "Mail Sent ".$working;
	}
	else {
		echo "Not Working";
	}
}

function email_carsold_cancellation_report($email_address,$salesmanager,$carid,$carnumber,$transactiontype,$dealerid,$dealershipname,$reasonforcancellation,$token,$tokenamount) {

	$sentFrom = 'prashantsinghbana673@gmail.com';
	$senderPassword = 'bububana2205';
		
    $body = '
    Hi All,

    The following sold car has been cancelled : 

    	<table style="border-radius:2px;margin:20px 0 25px;min-width:400px;border:1px solid #eee" cellspacing="0" cellpadding="10" border="0">
			<tbody>
				
				<tr style="background:#fbfbfb">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Email address</td>
					<td style="border-bottom:1px solid #eee"><a href="'.$email_address.'" target="_blank">'.$email_address.'</a></td>
				</tr>

				<tr style="background:#f9f9f9">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Sales Manager</td>
					<td style="border-bottom:1px solid #eee">'.$salesmanager.'</td>
				</tr>

				<tr style="background:#fbfbfb">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">CarID</td>
					<td style="border-bottom:1px solid #eee">'.$carid.'</td>
				</tr>

				<tr style="background:#f9f9f9">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Registration Number</td>
					<td style="border-bottom:1px solid #eee">'.$carnumber.'</td>
				</tr>

				<tr style="background:#fbfbfb">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Transaction Type</td>
					<td style="border-bottom:1px solid #eee">'.$transactiontype.'</td>
				</tr>

				<tr style="background:#f9f9f9">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Dealer ID</td>
					<td style="border-bottom:1px solid #eee">'.$dealerid.'</td>
				</tr>

				<tr style="background:#fbfbfb">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Dealership Name</td>
					<td style="border-bottom:1px solid #eee">'.$dealershipname.'</td>
				</tr>

				<tr style="background:#f9f9f9">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Reason for Cancellation</td>
					<td style="border-bottom:1px solid #eee">'.$reasonforcancellation.'</td>
				</tr>

				<tr style="background:#fbfbfb">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Token </td>
					<td style="border-bottom:1px solid #eee">'.$token.'</td>
				</tr>

				<tr style="background:#f9f9f9">
					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Token Amount</td>
					<td style="border-bottom:1px solid #eee">'.$tokenamount.'</td>
				</tr>
			</tbody>
		</table>';

	$recipient = 'prashant@bluejack.in';
	$recipient1 = 'divyanshu@bluejack.in';
    $mail = new PHPMailer();
	$mail->Host= "smtp.gmail.com";
	$mail->isSMTP();
	//	$mail->SMTPDebug  = 2;
	$mail->Subject = "Car Sale Cancellation Report | ".$carid;
	$mail->IsHTML(true);
	$mail->SMTPAuth = true;
	$mail->Username = $sentFrom;
	$mail->Password = $senderPassword;
	$mail->SMTPSecure ='tls';
	$mail->Port = 587;
	$mail->Subject = "Car Sale Cancellation Report | ".$carid;
	$mail->Body = $body;
	$mail-> setFrom($sentFrom,'Bluejack');
	$mail -> addAddress($recipient,'Prashant');
	$mail -> addAddress($recipient1,'Divyanshu');

	if($mail->send()){
		echo "Mail Sent ";
	}
	else {
		echo "Not Working";
	}
}

function email_carpurchase_cancellation_report($email_address,$spoc,$carid,$carnumber,$reasonforcancellation,$returnamount) {

	$sentFrom = 'prashantsinghbana673@gmail.com';
	$senderPassword = 'bububana2205';
		
    $body = '
    Hi All,

    The following purchased car has been cancelled : 

    	<table style="border-radius:2px;margin:20px 0 25px;min-width:400px;border:1px solid #eee" cellspacing="0" cellpadding="10" border="0">
    		<tbody>

    			<tr style="background:#fbfbfb">
    				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Email address</td>
    				<td style="border-bottom:1px solid #eee"><a href="mailto:saif.raza@bluejack.in" target="_blank">'.$email_address.'</a></td>
    			</tr>

    			<tr style="background:#f9f9f9">
    				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">SPOC</td>
    				<td style="border-bottom:1px solid #eee">'.$spoc.'</td>
    			</tr>

    			<tr style="background:#fbfbfb">
    				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">CarID</td>
    				<td style="border-bottom:1px solid #eee">'.$carid.'</td>
    			</tr>

    			<tr style="background:#f9f9f9">
    				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Car Registration Number</td>
    				<td style="border-bottom:1px solid #eee">'.$carnumber.'</td>
    			</tr>

    			<tr style="background:#fbfbfb">
    				<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Reason for Cancellation</td>
    				<td style="border-bottom:1px solid #eee">'.$reasonforcancellation.'</td>
    				</tr>

    				<tr style="background:#f9f9f9">
    					<td style="font-weight:bold;border-bottom:1px solid #eee;width:180px">Return Amount from Customer</td>
    					<td style="border-bottom:1px solid #eee">'.$returnamount.'</td>
    				</tr>
    			</tbody>
    		</table>
    	';

	$recipient = 'prashant@bluejack.in';
	$recipient1 = 'divyanshu@bluejack.in';
    $mail = new PHPMailer();
	$mail->Host= "smtp.gmail.com";
	$mail->isSMTP();
	//	$mail->SMTPDebug  = 2;
	$mail->Subject = "Car Purchase Cancellation Report | ".$carid;
	$mail->IsHTML(true);
	$mail->SMTPAuth = true;
	$mail->Username = $sentFrom;
	$mail->Password = $senderPassword;
	$mail->SMTPSecure ='tls';
	$mail->Port = 587;
	$mail->Subject = "Car Purchase Cancellation Report | ".$carid;
	$mail->Body = $body;
	$mail-> setFrom($sentFrom,'Bluejack');
	$mail -> addAddress($recipient,'Prashant');
	$mail -> addAddress($recipient1,'Divyanshu');

	if($mail->send()){
		echo "Mail Sent ";
	}
	else {
		echo "Not Working";
	}
}


?> 

